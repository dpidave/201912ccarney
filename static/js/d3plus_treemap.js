// this sets the order of the levels to display
var levels = ["level1", "level2", "level3"];
// sets the depth 0, 1, or 2 (for class, cat, res)
var levDepth = 0;

// setup the landing page
var visualization = d3plus.viz()
    .container("#viz")
    .data(data_test)
    .type("tree_map")
    .id(levels)
    .color("level1")
    .size("volume")
    .labels({
        "align": "left",
        "valign": "top",
        "resize": true,
        "fontFamily": "Roboto",
    })
  .tooltip({
    "share": true,    // turns off the "Share" value
    "size": true,     // turns off the "Size" value
    "children": true, // turns off the list of child nodes if multi-level
    "stacked": true,
})
    .title({
        "total": {
            "font": {"size": 18, 
                    "family": "Roboto", 
                    "color":"#000000"
                    },
            "value": {"prefix": "Total tonnes: "},
        },
    })
    .format({
        "text": function(text, key) {
            if (text.split(0, 3) == "MSW") {
                return text.toUpperCase();
            } else {
                return text;
            }
        },
        "number": function(number, params) {
          var formatted = d3plus.number.format(number, params);
          if (params.key === "volume") {
              return formatted; // + " (T)";
          } else {
              return formatted;
          }
        },
    })
    .color(function(d) {
      return d.color;
    })
    .zoom({
        "click": false,
        "value": false,
    })
    .font({ "family": "Roboto" })
    .depth(levDepth)
    .draw();
//
//
/**
 * This function updates the level to be displayed
* based on button click from the user.
* The level will break up the treemap into finer levels
* @param {string} result - a button flag from the caller
*/
function levFun(result) {
    // logic to change the depth displayed based on button push
    if (result == 'lev1') {
        levDepth = 0;
    } else if (result == 'lev2') {
        levDepth = 1;
    } else {
        levDepth = 2;
    }
    // update the the viz based on the new depth
    visualization.id(levels)
        .color("level1")
        .id(levels) // use all levels, but only show specific depth
        .depth(levDepth) // change depth here
        .size("volume")
        .format({"text": function(text, key) {
            if (text.split(0, 3) == "MSW") {
                return text.toUpperCase();
            } else {
                    return text;
            }
            },
        }) // required as d3plus converts MSW to Msw
        .color(function(d) {
          return d.color;
        })
        .font({ "family": "Roboto" })
        .labels({"align": "left", "valign": "top"})
        .draw();
} // end of levFun
