#!/usr/bin/env python

import csv
import sys

color_dict = {"Cropping": ("#ef9e8b","#5a1c0c"),
        "Forestry": ("#f1de81", "#453b08"),
        "Livestock": ("#abafd5", "#222644"),
        "Horticulture": ("#e492ff", "#4d0066" ),
        "Organic Waste": ("#8ebe8b", "#bfdabe")
        }
string_start = 'var data_test = ['
string_end = '];'
info = '' 
infile = sys.argv[1]
outfile = sys.argv[2]
with open(infile) as rf:
    csv_reader = csv.reader(rf)
    header = next(csv_reader)
    for row in csv_reader:
        info += '{level1: "%s",level2:"%s", level3:"%s", text: "%s", color: "%s", volume: %s},' % (row[0], row[1], row[2], 
                 color_dict[row[0]][1],color_dict[row[0]][0], row[3])

out = string_start + info[:-1] + string_end # remove trailing comma
print(out)
with open(outfile, 'w') as wf:
    wf.write(out)
