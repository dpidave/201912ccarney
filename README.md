# 201912CCARNEY  

## Introduction  
A basic <a href="https://d3plus.org/">D3plus</a> treemap for ABBA data.

## Branches  
- master - main version  
- dev - version that allows zooming  
- merge - attempt to merge master and dev zoom/split logic [Broken]  

## The file structure  

```
.
├── index.html
├── README.md
└── static
    ├── css
    │   └── style.css
    ├── data
    │   ├── ABBA-Biomass-Residues-Summary-For-Infographics.xlsx
    │   ├── data.js
    │   └── data_mod.csv
    ├── js
    │   ├── d3plus_treemap.js
    │   └── treemap.js
    └── scripts
        └── create_var.py
```

index.html - main html file  
static/data/data.js - main data file in a js object  
static/data/data_mod.csv - spreadsheet of data from Cath (with formatting)  
static/scripts/create_var.py - python script to process CSV into JS for the data.js creation.  
static/js/d3plus_treemap.js - main javascript file for d3plus magic.  

Any similar data can be displayed using this code if the format (column headings/ row formats) are followed as shown in the `static/data/data_mod.csv` file. If this changes the python script just needs to be run again to create a new `data.js` file that will be processed by javascript to create the webpage. To do this run (from the root directory).  

```
# run from the base directory
python scripts/create_var.py static/data/data_mod.csv static/data/data.js
```

## Tools  
Although the website was hosted at pythonanywhere.com for simple sharing, for testing a ran a virtual server on my Linux machine using python.  The command to launch this server was `python3 -m http.server`

I could then point my browser to `http://0.0.0.0:8000/index.html` to display the website.  
## 
Excellent d3 and d3plus javascript libraries.  
